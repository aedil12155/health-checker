import responses
from flexmock import flexmock

from app import producer
from app.producer import run_producer, get_site_data

sample_response_text = """
    <!DOCTYPE html>
    <html lang="en">
        <head><title>page title</title></head>
        <body>hello world</body>
    </html>
"""


@responses.activate
def test_get_site_data():
    topic = 'test'
    site_url = 'https://www.foodteller.de/'

    responses.add(
        responses.GET,
        site_url,
        body=sample_response_text,
        status=200,
        content_type='text/html',
    )

    response = get_site_data(site_url)
    assert response['response_status_code'] == 200
    assert 'pattern_matched' not in response

    # try with pattern
    response = get_site_data(site_url, pattern_regex='hello world')
    assert response['response_status_code'] == 200
    assert response['pattern_matched'] == 'hello world'
    assert 'pattern_matched' in response


def test_producer_run():
    topic = 'test'
    site_url = 'https://www.foodteller.de/'
    pattern = 'helloworld'

    mocked_health_response = {
        'response_time_milliseconds': 1,
        'response_status_code': 200,
        'requested_at': '2013-12-12 00:00:00',
        'pattern_matched': pattern
    }
    (flexmock(producer).should_receive('get_site_data')
     .with_args(site_url, pattern_regex=pattern)
     .and_return(mocked_health_response))

    mocked_kafka = flexmock()

    mocked_kafka.should_receive('send').with_args(topic, key=site_url,
                                                  value=mocked_health_response).and_return(True)
    mocked_kafka.should_receive('flush').and_return(True)
    flexmock(producer.KafkaProducer).new_instances(mocked_kafka)

    run_producer(site_url, 'http://1234.asd/', '/', topic, pattern_regex=pattern)
