#!/bin/sh

CRON_DURATION_MINUTES=${CRON_DURATION_MINUTES:-1}
PATTERN_REGEX=${PATTERN_REGEX:-""}

if [[ -z $SITE_URL || -z $KAFKA_SERVERS || -z $KAFKA_CERT_PATH || -z $KAFKA_TOPIC ]]; then
  echo 'Missing one or more environment variables (SITE_URL, KAFKA_CERT_PATH, KAFKA_SERVERS, KAFKA_TOPIC)'
  exit 1
fi

echo '*** Create topic'
python ./topic.py

echo '*** Copy the script run to crontab'
echo "*/${CRON_DURATION_MINUTES} * * * * python ${PROJ_DIR}/producer.py >> ${LOG_FILE} 2>&1" > $PROJ_DIR/crontab
crontab $PROJ_DIR/crontab
crontab -l
crond

tail -f ${LOG_FILE}
