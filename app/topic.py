import argparse
import os

from kafka.admin import KafkaAdminClient, NewTopic
from kafka.errors import TopicAlreadyExistsError


def run(kafka_servers, kafka_cert_path, kafka_topic):
    admin_client = KafkaAdminClient(
        bootstrap_servers=kafka_servers,
        security_protocol='SSL',
        ssl_cafile=f"{kafka_cert_path}/ca.pem",
        ssl_certfile=f"{kafka_cert_path}/service.cert",
        ssl_keyfile=f"{kafka_cert_path}/service.key",
    )

    topic_list = [NewTopic(name=kafka_topic, num_partitions=1, replication_factor=1)]
    try:
        admin_client.create_topics(new_topics=topic_list, validate_only=False)
    except TopicAlreadyExistsError:
        pass


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--kafka_servers', type=str, help='Kafka bootstrap servers!',
                        default=os.environ.get('KAFKA_SERVERS'))
    parser.add_argument('-t', '--kafka_topic', type=str, help='Kafka topic!',
                        default=os.environ.get('KAFKA_TOPIC'))
    parser.add_argument('-p', '--kafka_cert_path', type=str, help='Kafka certificate path!',
                        default=os.environ.get('KAFKA_CERT_PATH'))
    args_dict = vars(parser.parse_args())

    if not (args_dict['kafka_servers'] and args_dict['kafka_cert_path'] and args_dict['kafka_topic']):
        raise ValueError('Missing all the required argument (kafka_cert_path, kafka_servers, kafka_topic)')

    run(**args_dict)
