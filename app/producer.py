import argparse
import datetime
import json
import logging
import os
import re

import requests
from kafka import KafkaProducer

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


def get_site_data(site_url, pattern_regex=None):
    response = requests.get(site_url, allow_redirects=True)
    time_ms = response.elapsed.total_seconds() * 1000
    message = {"response_time_milliseconds": int(time_ms), "response_status_code": response.status_code,
               "requested_at": str(datetime.datetime.utcnow())}

    if pattern_regex and re.search(pattern_regex, response.text):
        message['pattern_matched'] = pattern_regex
    return message


def run_producer(site_url, kafka_servers, kafka_cert_path, kafka_topic, pattern_regex=None):
    producer = KafkaProducer(
        bootstrap_servers=kafka_servers,
        security_protocol='SSL',
        ssl_cafile=f"{kafka_cert_path}/ca.pem",
        ssl_certfile=f"{kafka_cert_path}/service.cert",
        ssl_keyfile=f"{kafka_cert_path}/service.key",
        value_serializer=lambda v: json.dumps(v).encode('utf-8'),
        key_serializer=lambda v: v.encode('utf-8'),
    )

    health_data = get_site_data(site_url=site_url, pattern_regex=pattern_regex)

    producer.send(kafka_topic, key=site_url, value=health_data)
    producer.flush()
    logger.info(f"Sent message: {health_data}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-u', '--site_url', type=str, help='Website URL to scrape!',
                        default=os.environ.get('SITE_URL'))
    parser.add_argument('-s', '--kafka_servers', type=str, help='Kafka bootstrap servers!',
                        default=os.environ.get('KAFKA_SERVERS'))
    parser.add_argument('-p', '--kafka_cert_path', type=str, help='Kafka certificate path!',
                        default=os.environ.get('KAFKA_CERT_PATH'))
    parser.add_argument('-t', '--kafka_topic', type=str, help='Kafka topic!',
                        default=os.environ.get('KAFKA_TOPIC'))
    parser.add_argument('-r', '--pattern_regex', type=str, help='Regex pattern to match on the site!',
                        default='PATTERN_REGEX')

    args_dict = vars(parser.parse_args())

    if not (args_dict['site_url'] and args_dict['kafka_servers'] and args_dict['kafka_cert_path']
            and args_dict['kafka_topic']):
        raise ValueError('Missing all the required argument (site_url, kafka_cert_path, kafka_servers, kafka_topic)')

    run_producer(**args_dict)
