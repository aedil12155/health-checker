import argparse
import logging
import os
import psycopg2

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


def run(pg_host, pg_port, pg_username, pg_password, pg_database):
    connection = psycopg2.connect(host=pg_host,
                                  database=pg_database,
                                  user=pg_username,
                                  password=pg_password,
                                  port=pg_port)

    create_table_command = """
        CREATE TABLE IF NOT EXISTS site_health_data (
            id SERIAL PRIMARY KEY,
            url VARCHAR(512) NOT NULL,
            response_code SMALLINT NOT NULL,
            response_time_ms SMALLINT NOT NULL,
            regex_pattern_matched VARCHAR(512) NULL,
            requested_at TIMESTAMP NOT NULL,
            created_at TIMESTAMP DEFAULT now()
        );
        """
    indexes = [
        """CREATE INDEX IF NOT EXISTS site_health_data_url_idx ON site_health_data(url);""",
    ]
    cursor = connection.cursor()
    cursor.execute(create_table_command)
    for index in indexes:
        cursor.execute(index)
        connection.commit()

    cursor.close()
    if connection is not None:
        connection.close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument('-n', '--pg_host', type=str, help='Postgres database host!',
                        default=os.environ.get('PG_HOST'))
    parser.add_argument('-p', '--pg_port', type=str, help='Postgres port!',
                        default=os.environ.get('PG_PORT'))
    parser.add_argument('-u', '--pg_username', type=str, help='Postgres username!',
                        default=os.environ.get('PG_USERNAME'))
    parser.add_argument('-w', '--pg_password', type=str, help='Postgres password!',
                        default=os.environ.get('PG_PASSWORD'))
    parser.add_argument('-d', '--pg_database', type=str, help='Postgres database!',
                        default=os.environ.get('PG_DATABASE_NAME'))

    args_dict = vars(parser.parse_args())

    if not (args_dict['pg_host'] and args_dict['pg_port'] and args_dict['pg_username']
            and args_dict['pg_password'] and args_dict['pg_database']):
        raise ValueError('Missing required argument (hostname, port, username, password, database)')

    run(**args_dict)
