import argparse
import json
import logging
import os
import psycopg2

from time import sleep

from kafka import KafkaConsumer

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

INSERT_SQL = """
INSERT INTO site_health_data(url, response_time_ms, response_code, regex_pattern_matched, requested_at) 
VALUES(%s, %s, %s, %s, %s)
"""


def run(kafka_servers, kafka_cert_path, kafka_topic, pg_host, pg_port, pg_username, pg_password, pg_database,
        group_id='test-group', auto_offset_reset='earliest'):
    consumer = KafkaConsumer(
        kafka_topic,
        bootstrap_servers=kafka_servers,
        security_protocol='SSL',
        ssl_cafile=f"{kafka_cert_path}/ca.pem",
        ssl_certfile=f"{kafka_cert_path}/service.cert",
        ssl_keyfile=f"{kafka_cert_path}/service.key",
        auto_offset_reset=auto_offset_reset,
        group_id=group_id,
        enable_auto_commit=True,
        key_deserializer=lambda v: v.decode('utf-8'),
        value_deserializer=lambda v: json.loads(v.decode('utf-8')),

    )
    connection = psycopg2.connect(host=pg_host,
                                  database=pg_database,
                                  user=pg_username,
                                  password=pg_password,
                                  port=pg_port)

    cursor = connection.cursor()
    for event in consumer:
        value = event.value
        logger.info(f'Event consumed: {value}')
        cursor.execute(INSERT_SQL, (event.key, value['response_time_milliseconds'],
                                    value['response_status_code'], value.get('pattern_matched'), value['requested_at']))
        connection.commit()
        logger.info(f'Event written to DB!')
        sleep(2)

    cursor.close()
    if connection is not None:
        connection.close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument('-s', '--kafka_servers', type=str, help='Kafka bootstrap servers!',
                        default=os.environ.get('KAFKA_SERVERS'))
    parser.add_argument('-c', '--kafka_cert_path', type=str, help='Kafka certificate path!',
                        default=os.environ.get('KAFKA_CERT_PATH'))
    parser.add_argument('-t', '--kafka_topic', type=str, help='Kafka topic!',
                        default=os.environ.get('KAFKA_TOPIC'))
    parser.add_argument('-n', '--pg_host', type=str, help='Postgres database host!',
                        default=os.environ.get('PG_HOST'))
    parser.add_argument('-p', '--pg_port', type=str, help='Postgres port!',
                        default=os.environ.get('PG_PORT'))
    parser.add_argument('-u', '--pg_username', type=str, help='Postgres username!',
                        default=os.environ.get('PG_USERNAME'))
    parser.add_argument('-w', '--pg_password', type=str, help='Postgres password!',
                        default=os.environ.get('PG_PASSWORD'))
    parser.add_argument('-d', '--pg_database', type=str, help='Postgres database!',
                        default=os.environ.get('PG_DATABASE_NAME'))

    args_dict = vars(parser.parse_args())

    if not (args_dict['kafka_servers'] and args_dict['kafka_cert_path']
            and args_dict['kafka_topic'] and args_dict['pg_port']
            and args_dict['pg_host'] and args_dict['pg_username']
            and args_dict['pg_password'] and args_dict['pg_database']):
        raise ValueError(f'Missing one or more required argument ({", ".join(args_dict.keys())})')

    run(**args_dict)
