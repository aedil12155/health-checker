#!/bin/sh

if [[ -z $KAFKA_SERVERS || -z $KAFKA_CERT_PATH || -z $KAFKA_TOPIC || -z $PG_HOST || -z $PG_PORT || -z $PG_USERNAME || -z $PG_PASSWORD || -z $PG_DATABASE_NAME ]]; then
  echo 'Missing one or more environment variables (KAFKA_CERT_PATH, KAFKA_SERVERS, KAFKA_TOPIC, PG_HOST, PG_PORT, PG_USERNAME, PG_PASSWORD, PG_DATABASE_NAME)'
  exit 1
fi

echo '*** Creating database table'
python ./create_table.py

echo '*** Starting consumer'
python ./consumer.py > ${LOG_FILE} &

tail -f ${LOG_FILE}
