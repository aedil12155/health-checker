Table of contents
=================

* [Setting up](#setting-up)
* [Run application](#run-application-in-docker)
* [Run test](#run-test)


## Setting up

Python 3.8 is required for this project, checkout [python website](https://www.python.org/downloads/) on how to setup.

### 1. Clone Project

    $ cd /path/to/your/workspace
    $ git clone ...


### 2. Setup Env

Make sure one have Docker installed.
Install [virtualenv](http://www.virtualenv.org/). Once you have virtualenv installed simply create virtualenv for the project.

    $ virtualenv --python=/path/to/python3.8 <virtualenv-name>
    $ source <virtualenv-name>/bin/activate # Activate environment
    $ pip install docker-compose  

Copy/rename `env.example` to `.env` and add the values for the required ENV variables accordingly,

    $ cp env.sample .env

In order to connect with kafka copy the certificates to `./kafka_cert` folder

## Run application in Docker
Once you have environment ready and ENV variables set you can simply run,

    $ docker-compose up

## Run test

    $ pip install -r requirements/test.txt
    $ py.test 
